import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { Map } from 'immutable';
import { composeWithDevTools } from 'remote-redux-devtools';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const initialState = new Map({
  test: 'wisp',
});
const reducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state
  }
}
const store = createStore(
  reducer,
  initialState, 
  composeWithDevTools({ realtime: true})(),
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
